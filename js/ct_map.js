var indicatorAjaxRequest = '';

jQuery(document).ready(function($) {

    $( "#block-indicatorcards" ).sortable({
        handle: ".grab",
        //scroll: true,
        axis: "y",
        scrollSpeed: 10
    });

    $(".indicator-actions-icon[aria-label='Minimise all layers']").tooltip();
    $(".indicator-actions-icon[aria-label='Close all layers']").tooltip();

    $( "#block-indicatorcards" ).on( "sortbeforestop", function( event, ui ) {
        $(".indicator-actions-icon").tooltip("hide");
        var nodeID = $(ui.item).find(".node_id").text().trim();
        //console.log($(ui.item).find(".map-layer-id").first().text());
        var thisID = ui.item[0].id;
        $().moveMapLayers(map, nodeID, "up"); 
        $().pushTopMapLayers(map); 
        
        var sortedIDs = $( "#block-indicatorcards" ).sortable( "toArray" );

        if (thisID == sortedIDs[0]) { //if it's first, it's at the top (move up)
            $().moveMapLayers(map, nodeID, "up"); 
            $().pushTopMapLayers(map); 
        } else { 
            //if it's not first, check the immediate following layer and move behind it. 
            var nextid = $("#"+thisID).prev().find(".node_id").text().trim();
            var layerid = $("#"+thisID).prev().find(".map-layer-id").first().text().trim();
            $().moveMapLayers(map, nodeID, layerid+"-b10p4m4"+nextid); 
        }
    } );

    $( "#block-indicatorcard" ).draggable({
        handle: ".grab-all",
        containment: "#map-container"
    });

    var collapseCards = document.getElementById("block-indicatorcards");
    collapseCards.addEventListener('shown.bs.collapse', function (e) {
        e.stopPropagation();
        $(this).closest("#block-indicatorcard").find("i.fa-window-maximize").first().removeClass("fa-window-maximize").addClass("fa-window-minimize");
        var tooltip = bootstrap.Tooltip.getInstance(".indicator-actions-icon[aria-label='Minimise all layers']");
        tooltip.setContent({ '.tooltip-inner': 'Minimise all layers' });
    });

    collapseCards.addEventListener('hide.bs.collapse', function (e) {
        e.stopPropagation();
        $(this).closest("#block-indicatorcard").find("i.fa-window-minimize").first().removeClass("fa-window-minimize").addClass("fa-window-maximize")
        var tooltip = bootstrap.Tooltip.getInstance(".indicator-actions-icon[aria-label='Minimise all layers']");
        tooltip.setContent({ '.tooltip-inner': 'Maximize all layers' });
    });

    Drupal.behaviors.IndicatorCard = {
		attach: function (context, settings) {
            var selector = context.querySelector(".indicator-content-wrapper");
            if (selector) {
                if (!selector.getAttribute('data-once')) {
                    selector.setAttribute('data-once', 'true');
                    var thisID = context.id;
                    var nodeID = thisID.replace('id-', '');
                    var blockIndicatorCards = document.getElementById("block-indicatorcards");

                    var collapseCard = document.getElementById("collapseCard"+nodeID);
                    collapseCard.addEventListener('shown.bs.collapse', function (e) {
                        e.stopPropagation();
                        $("#id-"+nodeID).find(".mini-title").addClass("d-none");
                        $("#id-"+nodeID).find("i.fa-window-maximize").removeClass("fa-window-maximize").addClass("fa-window-minimize")
                        var tooltip = bootstrap.Tooltip.getInstance("#id-" + nodeID + " .indicator-actions-icon[aria-label='Minimise']");
                        tooltip.setContent({ '.tooltip-inner': 'Minimize' });
                    });

                    collapseCard.addEventListener('hide.bs.collapse', function (e) {
                        e.stopPropagation();
                        $("#id-"+nodeID).find(".mini-title").removeClass("d-none");
                        $("#id-"+nodeID).find("i.fa-window-minimize").removeClass("fa-window-minimize").addClass("fa-window-maximize")
                        var tooltip = bootstrap.Tooltip.getInstance("#id-" + nodeID + " .indicator-actions-icon[aria-label='Minimise']");
                        tooltip.setContent({ '.tooltip-inner': 'Maximize' });
                    });

                    var layerUpTrigger = document.getElementById('layerUp'+nodeID);
                    if (layerUpTrigger) {
                        layerUpTrigger.addEventListener('click', () => {
                            //$(this).closest(".card.indicator-card").detach().prependTo("#block-indicatorcards");
                            context.parentNode.removeChild(context);
                            blockIndicatorCards.prepend(context);
                            $().moveMapLayers(map, nodeID, "up"); 
                            $().pushTopMapLayers(map); 
                        })
                    }

                    var layerDownTrigger = document.getElementById('layerDown'+nodeID);
                    if (layerDownTrigger) {
                        layerDownTrigger.addEventListener('click', () => {
                            //$(this).closest(".card.indicator-card").detach().appendTo("#block-indicatorcards");
                            context.parentNode.removeChild(context);
                            blockIndicatorCards.append(context);
                            $().moveMapLayers(map, nodeID, "down"); 
                        })
                    }

                    var performStatisticsTrigger = document.getElementById('performStatistics'+nodeID);
                    if (performStatisticsTrigger) {
                        performStatisticsTrigger.addEventListener('click', () => {
                            var someTabTriggerEl;
                            // var currentPath = window.location.pathname;
                            if((selSettings.WDPAID !== null) && ($("#local-tab-" + nodeID + ":visible").length)){
                                someTabTriggerEl = document.getElementById("local-tab-" + nodeID);
                            } else if((selSettings.iso2 !== null) && ($("#national-tab-" + nodeID + ":visible").length)){
                                someTabTriggerEl = document.getElementById("national-tab-" + nodeID);
                            } else if((selSettings.regionID !== null) && ($("#regional-tab-" + nodeID + ":visible").length)){
                                someTabTriggerEl = document.getElementById("regional-tab-" + nodeID);
                            } else {
                                someTabTriggerEl = document.getElementById("global-tab-" + nodeID);
                            }
                            var tab = new bootstrap.Tab(someTabTriggerEl);
                            tab.show();
                        })
                        performStatisticsTrigger.addEventListener('shown.bs.collapse', function (e) {
                            e.stopPropagation();
                        });
                        performStatisticsTrigger.addEventListener('hide.bs.collapse', function (e) {
                            e.stopPropagation();
                        });
                    }

                    return true;
                } 
            }
		}
	};



    $(".check-filter").on('click', function (event) {
        event.preventDefault();
        console.log($("label", this).text().trim())
        if ($("label", this).text().trim() == "European Commission Datasets"){
            if ($("input", this).prop("checked")) {
                $("input", this).prop('checked', false);
                $("#menu-indicator-cards .source-ec-jrc").parent().addClass("d-none");
            } else {
                $("input", this).prop('checked', true);
                $("#menu-indicator-cards .source-ec-jrc").parent().removeClass("d-none");
            }
        }

        if ($("label", this).text().trim() == "Third Parties Datasets"){
            if ($("input", this).prop("checked")) {
                $("input", this).prop('checked', false);
                $("#menu-indicator-cards .indicator-card:not(.source-ec-jrc)").parent().addClass("d-none");
            } else {
                $("input", this).prop('checked', true);
                $("#menu-indicator-cards .indicator-card:not(.source-ec-jrc)").parent().removeClass("d-none");
            }   
        }
        
    });

    var map;
    if($("#map-container").length){ //we are checking if the map div is the one from the CTT page
        map = $().createMap("map-container");

        $().addMapControls(map, "satelliteToggle");
        $().addMapControls(map, "zoom");
        $().addMapControls(map, "hoveredAreaLabelBlock");
        //$().addMapControls(map, "paPolyFillControl");
        $().addMapControls(map, "loaderControl");
        $().addMapControls(map, "fullScreen");
        $().addMapControls(map, "navigation");	
        $().addMapControls(map, "projection");
    
        $().addMapLayerBiopamaSources(map);
            
        //$().addMapLayer(map, "biopamaGaulEez");
        $().addMapLayer(map, "biopamaCountries");
        
        $().addMapLayer(map, "satellite");
        $().addMapLayer(map, "nan-layers");
        $().addMapLayer(map, "akp");
        //console.log(map.style);
        
        //$('[data-toggle="tooltip"]').tooltip();

        //var mask = L.tileLayer('https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/africa_platform:world_flat_no_africa_no_eez_@EPSG:900913@png/{z}/{x}/{y}.png', { tms: true, zIndex: 40, opacity: 1 }).addTo(map)

        // var popupOptions = {
        //     countryLink: '/ct/country/',
        //     paLink: '/ct/pa/',
        // }
        // $().addMapLayerInteraction(map, popupOptions);

    } else { // if it's not the CTT div we can assume we are on the Country/PA page and use that map instead.
        map = mymap;
    };
	
	$('input').each(function(i){
	  if(this.id){
		this.id = this.id+i;
		$(this).closest('form').addClass(this.id);
	  }
	});

	map.on('moveend', function () {
		//this flag can only be true in this case if the Protected Area has been changed to one in a different country from the search
		if (countryChanged === 1){	
            countryChanged = 0;
			updateCountry();
		}
		if (paChanged === 1){	
			updatePa();
		}
		if (regionChanged === 1){	
			updateRegion();
		}
	});
	
	map.on('load', function () {
        if (window.location.search.indexOf('?search=') > -1) { // check if we got here with a search term in the URL (to open an indicator by default)
            $("#menu-indicator-cards .indicator-card:first .menu-open-indicator").click(); //find the open indicator button of the first search result and click it.
        }
		$('body').toggleClass('loaded').delay( 500 ).queue(function() {
		  $('.mapboxgl-ctrl.ajax-loader').toggle(false);
          
		  mapPostLoadOptions();
		  $( this ).dequeue();
		});

        // const layers = map.getStyle().layers;
        // let firstSymbolId;
        // for (const layer of layers) {
        //     if (layer.type === 'symbol') {
        //         firstSymbolId = layer.id;
        //         break;
        //     }
        // }
        
	});

	function mapPostLoadOptions() {
		map.setMinZoom(1.4);
		map.setMaxZoom(16);
        //$().pushTopMapLayers(map); 
        //$('[data-toggle="tooltip"]').tooltip();
	}

});

function getRestResults(nodeID = 0){
    jQuery().insertBiopamaLoader("#id-"+nodeID+" .indicator-chart"); 
    indictorGlobalSettings[nodeID].firstChartRun = 1;
    var dataCountry = 1;
    //delete any errors that might be up, if they persist, they will be re-added
    jQuery( ".rest-error" ).empty();
    //if the chosen indicator has countries attached to it we highlight them, and mask the ones not included.
    if (selData.info.countries != ''){
        thisMap.setFilter("CountriesBadMask", buildFilter(selData.info.countries, '!in', 'iso3'));
        thisMap.setFilter("CountriesGoodMask", buildFilter(selData.info.countries, 'in', 'iso3'));
        thisMap.setLayoutProperty("CountriesGoodMask", 'visibility', 'visible');
        thisMap.setLayoutProperty("CountriesBadMask", 'visibility', 'visible');
    }
    if (selData.data.countries.length > 0){
        if (selData.data.countries.indexOf(selSettings.ISO3) > -1){
            // a country or countries have been set and the current country is in the set
            dataCountry = 1;
        } else {
            //a country or countries have been set and the currently selected country is not one of them
            dataCountry = 0;
        }
    }
    if (nodeID == 0){
        nodeID = jQuery("#block-indicatorcards .card").first().find(".node_id").text().trim();     
    }
    if (dataCountry == 1){

        //generate url by replacing tokens that might be in it.
        var indicatorURL = '';
        indicatorURL = jQuery().biopamaReplaceTokens(selData.chart.RESTurl, selSettings); 
        console.log(indicatorURL) 
        
        indicatorAjaxRequest = jQuery.ajax({
            url: indicatorURL,
            dataType: 'json',
            success: function(d) {
                jQuery().removeBiopamaLoader("#id-"+nodeID+" .indicator-chart"); 
                if (d.hasOwnProperty("records")){ //from a JRC REST Service
                    selData.chart.RESTResults = d.records;
                    if (d.metadata.recordCount == 0) {
                        //we create a card, but tell it that the response was empty (error 2)
                        getChart(2, nodeID);
                    } else {
                        //the 0 means there was no error
                        getChart(0, nodeID);
                    }
                } else if (d.hasOwnProperty(selData.chart.RESTdataContext)){ //single value passed, found in root, good to go.
                    selData.chart.RESTResults = d[selData.chart.RESTdataContext];
                    getChart(0, nodeID);
                } else if (selData.chart.RESTdataContext !== ''){
                    //var match1 = jsonPath(d, selData.chart.RESTdataContext); jsonPath works too, but JMES has better documentation
                    var match1 = jmespath.search(d, selData.chart.RESTdataContext);
                    //console.log(match1);
                    selData.chart.RESTResults = match1;
                    getChart(0, nodeID);
                } else {
                    selData.chart.RESTResults = d;
                    getChart(0, nodeID);
                }
            },
            error: function() {
                //jQuery().removeBiopamaLoader("#id-"+nodeID+" .indicator-chart"); 
                console.log("ERROR")
                //we create a card, but tell it that there was a general error (error 1)
                //todo - expand error codes to tell user what went wrong.
                getChart(1, nodeID);
            }
        });
    } else {
        jQuery().removeBiopamaLoader("#id-"+nodeID+" .indicator-chart"); 
        //we run the get chart function, only to show the user that a different country must be selected
        getChart(3, nodeID);
    }
    //AKP
}
/**
 * @file
 * AKP Partners map
 *
 */

(function ($, Drupal) {
    Drupal.behaviors.refreshPartnersMapView = {
		attach: function (context, settings) {
			$( ".view-akp-partners" ).once('build-map').each(this.buildMap);
		},
        buildMap: function (idx, column) {
 
            var map;
            if($("#map-container").length){ //we are checking if the map div is the one from the CTT page
                map = $().createMap("map-container");
                $().addMapControls(map, "zoom");
                $().addMapControls(map, "hoveredAreaLabelBlock");
                $().addMapControls(map, "paPolyFillControl");
                $().addMapControls(map, "loaderControl");
                $().addMapControls(map, "fullScreen");
                $().addMapControls(map, "navigation");	
            
                $().addMapLayerBiopamaSources(map);
                    
                //$().addMapLayer(map, "biopamaGaulEez");
                $().addMapLayer(map, "biopamaCountries");
        
        
            } else { // if it's not the CTT div we can assume we are on the Country/PA page and use that map instead.
                map = mymap;
            };
        
            const features = [];
        
            $('tbody tr').each(function() {
                var thisLngLat = $(this).find(".views-field-field-source-location").text().trim().split(", ");
                //console.log(thisLngLat);
                if (thisLngLat.length == 1){ return true; }
                var thisFeature = {
                    'type': 'Feature',
                    'properties': {
                        'popupMessage': $(this).find(".views-field-nothing").html().trim(),
                    },
                    'geometry': {
                        'type': 'Point',
                        'coordinates': thisLngLat
                    }
                }
        
                features.push(thisFeature)
            });
        
            const geojson = {
                'type': 'FeatureCollection',
                'features': features
            };
        
            map.on('load', () => {
        
                map.loadImage('https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Cat_silhouette.svg/400px-Cat_silhouette.svg.png', (error, image) => {
                    if (error) throw error;
                    if (!map.hasImage('theatre')) map.addImage('theatre', image);
                });
        
                map.addSource('places', {
                    'type': 'geojson',
                    'data': geojson,
                    cluster: true,
                    clusterMaxZoom: 14, // Max zoom to cluster points on
                    clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
                })
        
                map.addLayer({
                    id: 'clusters',
                    type: 'circle',
                    source: 'places',
                    filter: ['has', 'point_count'],
                    paint: {
                    // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
                    // with three steps to implement three types of circles:
                    //   * Blue, 20px circles when point count is less than 100
                    //   * Yellow, 30px circles when point count is between 100 and 750
                    //   * Pink, 40px circles when point count is greater than or equal to 750
                    'circle-color': [
                    'step',
                    ['get', 'point_count'],
                    '#51bbd6',
                    100,
                    '#f1f075',
                    750,
                    '#f28cb1'
                    ],
                    'circle-radius': [
                    'step',
                    ['get', 'point_count'],
                    20,
                    100,
                    30,
                    750,
                    40
                    ]
                    }
                });
        
                map.addLayer({
                    id: 'cluster-count',
                    type: 'symbol',
                    source: 'places',
                    filter: ['has', 'point_count'],
                    layout: {
                    'text-field': ['get', 'point_count_abbreviated'],
                    'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                    'text-size': 12
                    }
                });
        
                map.addLayer({
                    id: 'unclustered-point',
                    type: 'circle',
                    source: 'places',
                    filter: ['!', ['has', 'point_count']],
                    paint: {
                    'circle-color': '#11b4da',
                    'circle-radius': 8,
                    'circle-stroke-width': 1,
                    'circle-stroke-color': '#fff'
                    }
                });
        
                // inspect a cluster on click
                map.on('click', 'clusters', (e) => {
                    const features = map.queryRenderedFeatures(e.point, {
                    layers: ['clusters']
                    });
                    const clusterId = features[0].properties.cluster_id;
                    map.getSource('places').getClusterExpansionZoom(
                    clusterId,
                    (err, zoom) => {
                    if (err) return;
                    
                    map.easeTo({
                    center: features[0].geometry.coordinates,
                    zoom: zoom
                    });
                    }
                    );
                });
        
                // When a click event occurs on a feature in
                // the unclustered-point layer, open a popup at
                // the location of the feature, with
                // description HTML from its properties.
                map.on('click', 'unclustered-point', (e) => {
                    const coordinates = e.features[0].geometry.coordinates.slice();
                    const description = e.features[0].properties.popupMessage;
                    
                    // Ensure that if the map is zoomed out such that
                    // multiple copies of the feature are visible, the
                    // popup appears over the copy being pointed to.
                    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                    }
                    
                    new mapboxgl.Popup()
                    .setLngLat(coordinates)
                    .setHTML(description)
                    .addTo(map);
                });
                    
                map.on('mouseenter', 'clusters', () => {
                    map.getCanvas().style.cursor = 'pointer';
                });
        
                map.on('mouseleave', 'clusters', () => {
                    map.getCanvas().style.cursor = '';
                });
             //     // Change the cursor to a pointer when the mouse is over the places layer.
                map.on('mouseenter', 'unclustered-point', () => {
                    map.getCanvas().style.cursor = 'pointer';
                });
                     
                    // Change it back to a pointer when it leaves.
                map.on('mouseleave', 'unclustered-point', () => {
                    map.getCanvas().style.cursor = '';
                });
            });
       
            
        }
	};

})(jQuery, Drupal);